package cl.walmart.challenge;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import org.jboss.logging.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static io.restassured.RestAssured.get;
import static org.mockito.Mockito.when;

@QuarkusTest
public class SearchEndpointTest {

    private static final Logger LOGGER = Logger.getLogger(SearchEndpointTest.class);

    @InjectMock
    ProductRepository productRepository;

    @BeforeEach
    void setup() {
        when(productRepository.findByProductId("41"))
                .thenReturn(new ProductEntity(41,
                        "Ferrati",
                        "Automóvil eléctrico para niños 8 años",
                        "www.lider.cl/catalogo/images/catalogo_no_photo.jpg",
                        389990));

        when(productRepository.findProducts("televi"))
                .thenReturn(new ArrayList<>(List.of(
                        new ProductEntity(1,
                                "Marca1",
                                "Televisión 54''",
                                "www.lider.cl/catalogo/images/catalogo_no_photo.jpg",
                                80000),
                        new ProductEntity(44,
                                "Televi",
                                "Televisión 80 pulgadas QLED libre región, Maracaná",
                                "www.lider.cl/catalogo/images/catalogo_no_photo.jpg",
                                389990),
                        new ProductEntity(45,
                                "Televi",
                                "Televisión 80 QLED, Rio",
                                "www.lider.cl/catalogo/images/catalogo_no_photo.jpg",
                                2500000)
                )));
    }


    @Test
    public void findByProductId() {
        LOGGER.infov("Begin test find by product id");

        LOGGER.infov("Find product with id 11");
        ProductEntity[] productEntities1 = get("/search?q=11")
                .then()
                .statusCode(200)
                .extract()
                .as(ProductEntity[].class);
        LOGGER.infov("Result: {0}", Arrays.asList(productEntities1));
        Assertions.assertEquals(0, productEntities1.length);

        LOGGER.infov("Find product with id 41");
        ProductEntity[] productEntities = get("/search?q=41")
                .then()
                .statusCode(200)
                .extract()
                .as(ProductEntity[].class);
        LOGGER.infov("Result: {0}", Arrays.asList(productEntities));

        Assertions.assertEquals(1, productEntities.length);

        ProductEntity productEntity = Arrays.stream(productEntities).findFirst().get();
        Assertions.assertEquals(50, productEntity.discount);
        Assertions.assertEquals(194995, productEntity.discountedPrice);
    }

    @Test
    public void findByText() {
        LOGGER.infov("Begin test find by text");

        LOGGER.infov("Find text: 'ac'");
        ProductEntity[] productEntities2 = get("/search?q=ac")
                .then()
                .statusCode(200)
                .extract()
                .as(ProductEntity[].class);
        LOGGER.infov("Result: {0}", Arrays.asList(productEntities2));

        Assertions.assertEquals(0, productEntities2.length);

        LOGGER.infov("Find text: 'acme'");
        ProductEntity[] productEntities3 = get("/search?q=acme")
                .then()
                .statusCode(200)
                .extract()
                .as(ProductEntity[].class);
        LOGGER.infov("Result: {0}", Arrays.asList(productEntities3));

        Assertions.assertEquals(0, productEntities3.length);

        LOGGER.infov("Find text: 'televi'");
        ProductEntity[] productEntities = get("/search?q=televi")
                .then()
                .statusCode(200)
                .extract()
                .as(ProductEntity[].class);
        List<ProductEntity> productEntities1 = Arrays.asList(productEntities);
        LOGGER.infov("Result: {0}", productEntities1);

        Assertions.assertEquals(3, productEntities1.size());


        Assertions.assertEquals(20, productEntities1.get(0).discount);
        Assertions.assertEquals(64000, productEntities1.get(0).discountedPrice);

        Assertions.assertEquals(50, productEntities1.get(1).discount);
        Assertions.assertEquals(194995, productEntities1.get(1).discountedPrice);

        Assertions.assertEquals(40, productEntities1.get(2).discount);
        Assertions.assertEquals(1500000, productEntities1.get(2).discountedPrice);
    }
}
