package cl.walmart.challenge;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import org.jboss.logging.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@QuarkusTest
public class ProductServiceTest {

    private static final Logger LOGGER = Logger.getLogger(ProductServiceTest.class);

    @Inject
    ProductService productService;

    @InjectMock
    ProductRepository productRepository;

    @BeforeEach
    void setup() {
        when(productRepository.findByProductId("41"))
                .thenReturn(new ProductEntity(41,
                        "Ferrati",
                        "Automóvil eléctrico para niños 8 años",
                        "www.lider.cl/catalogo/images/catalogo_no_photo.jpg",
                        389990));

        when(productRepository.findProducts("televi"))
                .thenReturn(new ArrayList<>(List.of(
                        new ProductEntity(1,
                                "Marca1",
                                "Televisión 54''",
                                "www.lider.cl/catalogo/images/catalogo_no_photo.jpg",
                                80000),
                        new ProductEntity(44,
                                "Televi",
                                "Televisión 80 pulgadas QLED libre región, Maracaná",
                                "www.lider.cl/catalogo/images/catalogo_no_photo.jpg",
                                389990),
                        new ProductEntity(45,
                                "Televi",
                                "Televisión 80 QLED, Rio",
                                "www.lider.cl/catalogo/images/catalogo_no_photo.jpg",
                                2500000)
                )));
    }

    @Test
    void findByProductId() {
        LOGGER.infov("Begin test find by product id");

        LOGGER.infov("Find product with id 11");
        List<ProductEntity> productEntities1 = productService.searchProducts("11");
        LOGGER.infov("Result: {0}", productEntities1);
        Assertions.assertTrue(productEntities1.isEmpty());

        LOGGER.infov("Find product with id 41");
        List<ProductEntity> productEntities = productService.searchProducts("41");
        LOGGER.infov("Result: {0}", productEntities);
        Assertions.assertEquals(1, productEntities.size());

        Assertions.assertEquals(50, productEntities.get(0).discount);
        Assertions.assertEquals(194995, productEntities.get(0).discountedPrice);
    }

    @Test
    void findByText() {
        LOGGER.infov("Begin test find by text");

        LOGGER.infov("Find text: 'ac'");
        List<ProductEntity> ac = productService.searchProducts("ac");
        LOGGER.infov("Result: {0}", ac);
        Assertions.assertTrue(ac.isEmpty());

        LOGGER.infov("Find text: 'acme'");
        List<ProductEntity> acme = productService.searchProducts("acme");
        LOGGER.infov("Result: {0}", acme);
        Assertions.assertTrue(acme.isEmpty());

        LOGGER.infov("Find text: 'televi'");
        List<ProductEntity> productEntities = productService.searchProducts("televi");
        LOGGER.infov("Result: {0}", productEntities);

        Assertions.assertEquals(3, productEntities.size());

        Assertions.assertEquals(20, productEntities.get(0).discount);
        Assertions.assertEquals(64000, productEntities.get(0).discountedPrice);

        Assertions.assertEquals(50, productEntities.get(1).discount);
        Assertions.assertEquals(194995, productEntities.get(1).discountedPrice);

        Assertions.assertEquals(40, productEntities.get(2).discount);
        Assertions.assertEquals(1500000, productEntities.get(2).discountedPrice);
    }
}
