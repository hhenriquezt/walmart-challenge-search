import http from "../http-common";

class SearchDataService {
    
    search(searchTxt) {
        return http.get(`/search?q=${searchTxt}`);
    }
}

export default new SearchDataService();