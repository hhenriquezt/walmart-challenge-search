import Vue from "vue";

export const store = Vue.observable({
    productList: [],
    searchTxt: "",
    isEmptyResult: false
});

export const mutations = {
    setData(payload) {
        store.productList = payload;
        store.isEmptyResult = payload.length === 0;
    },
    setSearchTxt(text) {
        store.searchTxt = text;
    }
}