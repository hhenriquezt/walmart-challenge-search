package cl.walmart.challenge;

import com.mongodb.client.FindIterable;
import com.mongodb.client.model.Filters;
import io.quarkus.mongodb.panache.PanacheMongoRepository;
import org.bson.conversions.Bson;

import javax.enterprise.context.ApplicationScoped;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class ProductRepository implements PanacheMongoRepository<ProductEntity> {

    /**
     * Find product by id (product id not mongo id)
     * @param productId: product id
     * @return product
     */
    public ProductEntity findByProductId(String productId) {
        return find("id", Integer.parseInt(productId))
                .firstResult();
    }

    /**
     * Find products by text in brand & description properties
     * @param searchTxt: text to search
     * @return product list
     */
    public List<ProductEntity> findProducts(String searchTxt) {
        List<ProductEntity> products = new ArrayList<>();

        if (!searchTxt.isBlank() && searchTxt.length() > 3) {
            Bson filter = buildQuery(searchTxt);

            FindIterable<ProductEntity> findIterable = mongoCollection().find(filter, ProductEntity.class);
            for (ProductEntity entity : findIterable) {
                products.add(entity);
            }
        }

        return products;
    }

    /**
     * Build mongo query filters
     * @param searchTxt: text to search
     * @return mongo filter
     */
    private Bson buildQuery(String searchTxt) {
        return Filters.or(
                Filters.regex("description", searchTxt, "i"),
                Filters.regex("brand", searchTxt, "i")
        );
    }
}
