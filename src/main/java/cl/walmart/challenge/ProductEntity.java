package cl.walmart.challenge;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.PanacheMongoEntityBase;
import org.bson.codecs.pojo.annotations.BsonIgnore;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

import java.util.Objects;

@MongoEntity(collection = "products")
public class ProductEntity extends PanacheMongoEntityBase {
    @JsonIgnore
    @BsonProperty("_id")
    public ObjectId mongoId;

    @BsonProperty("id")
    public int id;
    public String brand;
    public String description;
    public String image;
    public int price;
    @BsonIgnore
    public int discountedPrice;
    @BsonIgnore
    public int discount;

    public ProductEntity() {
    }

    public ProductEntity(int id, String brand, String description, String image, int price) {
        this.id = id;
        this.brand = brand;
        this.description = description;
        this.image = image;
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductEntity that = (ProductEntity) o;
        return mongoId.equals(that.mongoId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mongoId);
    }

    @Override
    public String toString() {
        return "ProductEntity{" +
                "_id=" + mongoId +
                ", id=" + id +
                ", brand='" + brand + '\'' +
                ", description='" + description + '\'' +
                ", image='" + image + '\'' +
                ", price=" + price +
                ", discountedPrice=" + discountedPrice +
                ", discount=" + discount +
                '}';
    }
}
