package cl.walmart.challenge;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@ApplicationScoped
public class ProductService {

    private final Pattern pattern = Pattern.compile("-?\\d+(\\.\\d+)?");

    @Inject
    ProductRepository productRepository;

    /**
     * search products by text
     * @param searchTxt: text to search
     * @return product list
     */
    public List<ProductEntity> searchProducts(@NotNull String searchTxt) {
        List<ProductEntity> products = new ArrayList<>();

        if (isProductId(searchTxt)) {
            ProductEntity productEntity = productRepository.findByProductId(searchTxt);
            if (productEntity != null) products.add(productEntity);
        } else {
            products.addAll(productRepository.findProducts(searchTxt));
        }

        return checkProdDiscount(products);
    }

    /**
     * Check product discount by repeated chars in description
     * @param products: product list
     * @return list of products with discounted price (if apply)
     */
    private List<ProductEntity> checkProdDiscount(List<ProductEntity> products) {
        return products.stream()
                .peek(productEntity -> {
                    int discount = getDiscount(normalizeDescription(productEntity.description));
                    if (discount > 0) {
                        productEntity.discountedPrice = productEntity.price - ((productEntity.price * discount) / 100);
                        productEntity.discount = discount;
                    }

                })
                .collect(Collectors.toList());
    }

    /**
     * Get discount value based on repeated chars in description
     * @param description: product description
     * @return discount value
     */
    private int getDiscount(String description) {
        AtomicInteger discount = new AtomicInteger();
        Map<Character, Integer> charMap = new HashMap<>();

        for (char c : description.toCharArray()) {
            if (Character.isLetterOrDigit(c)) {
                if (charMap.containsKey(c)) {
                    int counter = charMap.get(c);
                    charMap.put(c, ++counter);
                } else {
                    charMap.put(c, 1);
                }
            }
        }

        charMap.forEach( (k, v) -> {
            if (v > 1) discount.incrementAndGet();
        });

        int i = discount.get();
        return (i >= 5) ? 50 : i * 10;
    }

    /**
     * remove accents
     * @param description: product description
     * @return normalized description in uppercase
     */
    private String normalizeDescription(String description) {
        description = Normalizer.normalize(description, Normalizer.Form.NFKD);

        return description.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "")
                .toUpperCase(Locale.ROOT);
    }


    /**
     * Check if text input is id
     * @param input: text to check
     * @return true if input is numeric and length <= 3
     */
    private boolean isProductId(String input) {
        boolean isNumber = false;

        if (!input.isBlank() && input.length() <= 3) {
            isNumber = pattern.matcher(input).matches();
        }

        return isNumber;
    }
}
