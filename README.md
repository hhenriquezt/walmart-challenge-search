# wallmart-challenge-search project

This project uses Quarkus, the Supersonic Subatomic Java Framework.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

The front page uses Vue.js (version 2) https://vuejs.org/

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:
```shell script
./mvnw compile quarkus:dev
```

## Packaging and running the application

The application can be packaged using:
```shell script
./mvnw package
```

## Running test
```shell script
./mvnw test
```

## Demo app
Check a live version of this app:
http://18.228.23.163:8080/

(press enter to search)


## Docker
There is a public docker image with this app:
hhenriquez/wallmart-challenge-search:1.0

To run the docker container (it assumes you have the mongodb container running and named as mongodb-local)
```shell script
docker pull hhenriquez/wallmart-challenge-search:1.0
docker run -i --rm --name wlm-chllenge --link mongodb-local:mongodb -e MONGODB_STRING=mongodb://brandDiscountsUser:brandDiscountsPassword@mongodb:27017 -p 8080:8080 hhenriquez/wallmart-challenge-search:1.0
```
